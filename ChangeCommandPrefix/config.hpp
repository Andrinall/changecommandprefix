using namespace std::string_literals;
char* Config::Path_folder()
{
	char szPath[MAX_PATH] = {};
	GetModuleFileNameA(NULL, szPath, MAX_PATH);
	char* lstChr = strrchr(szPath, '\\');
	*lstChr = '\0';
	return szPath;
}

std::string Config::ReadString(std::string _Section, std::string _Key)
{
	char str[256];
	GetPrivateProfileStringA(_Section.c_str(), _Key.c_str(), NULL, str, sizeof(str), (Path_folder() + "\\ChangeCommandPrefix.ini"s).c_str());
	return std::string(str);
}

int Config::ReadInt(std::string _Section, std::string _Key)
{
	char str[256];
	GetPrivateProfileStringA(_Section.c_str(), _Key.c_str(), NULL, str, sizeof(str), (Path_folder() + "\\ChangeCommandPrefix.ini"s).c_str());
	return std::stoi(str);
}
