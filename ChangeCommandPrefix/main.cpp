#include "pch.h"
#include <iostream>
#include <sstream>
#include <Windows.h>
#include <string>
#include "main.h"
#include "library.hpp"
#include "config.hpp"

auto AsiPlugin::string_to_hex(std::string input)
{
    std::stringstream ss;
    for (auto i = 0; i < input.size(); i++)
    {
        ss << std::hex << (int)input[i];
    }
    unsigned short byte = 0;
    std::istringstream iss(ss.str());
    iss >> std::hex >> byte;
    return byte % 0x100;
}

void AsiPlugin::WriteMemory(void* address, void* bytes, int byteSize)
{
    DWORD NewProtection;
    VirtualProtect(address, byteSize, PAGE_EXECUTE_READWRITE, &NewProtection);
    memcpy(address, bytes, byteSize);
    VirtualProtect(address, byteSize, NewProtection, &NewProtection);
}

AsiPlugin::AsiPlugin() {
    BYTE bytes[3] = { 0x80, 0x3B, AsiPlugin::string_to_hex(Config.ReadString("main", "key")) }; // cmp byte ptr [ebx], 2F
    switch (SAMP::GetSAMPVersion())
    {
    case SAMP::sampVersion::R1:
        address = 0x65d8b;
        break;
    case SAMP::sampVersion::R2:
        address = 0x65E5B;
        break;
    case SAMP::sampVersion::R3:
        address = 0x692BB;
        break;
    case SAMP::sampVersion::R4:
        address = 0x699EB;
        break;
    }
    AsiPlugin::WriteMemory(reinterpret_cast<void*>(SAMP::GetSAMPHandle() + address), bytes, 3);
}

AsiPlugin::~AsiPlugin() {
    AsiPlugin::WriteMemory(reinterpret_cast<void*>(SAMP::GetSAMPHandle() + address), (void*)"\x80\x3B\x2f", 3);
}

