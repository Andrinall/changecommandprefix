class AsiPlugin
{
public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();
protected:
	auto string_to_hex(std::string input);
	void WriteMemory(void* address, void* bytes, int byteSize);
	int address = NULL;
} AsiPlugin;


class Config
{
public:
	std::string ReadString(std::string _Section, std::string _Key);
	int ReadInt(std::string _Section, std::string _Key);
private:
	char* Path_folder();
} Config;


namespace SAMP
{
	enum class sampVersion : int
	{
		unknown = 0,
		notLoaded = -1,
		R1 = 1,
		R2 = 2,
		R3 = 3,
		R4 = 4,
		DL = 5
	};
	unsigned long GetSAMPHandle();
	enum sampVersion GetSAMPVersion();
	bool IsSampInitialized();
};
